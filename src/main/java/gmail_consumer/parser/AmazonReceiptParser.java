package gmail_consumer.parser;

import com.google.api.client.util.DateTime;
import com.google.api.services.gmail.model.Message;
import dto.ReceiptDTO;
import gmail_consumer.Retailer;

import java.util.List;

import static gmail_consumer.parser.ParserCommons.getDateText;
import static gmail_consumer.parser.ParserCommons.getSubjectText;

public class AmazonReceiptParser implements RetailerParser {
    @Override
    public ReceiptDTO getReceiptForMessage(Message payloadMessage) {
        //TODO - extract more info. Additional headers can be found in payloadMessage.getPayload().getHeaders()
        // call Conversions.mimeToUtf8(string) to convert gmail encoded messages

        String retailerName = Retailer.AMAZON.trademark;
        String invoiceId = getInvoiceId(payloadMessage);
        List<Long> itemsBought = null;   //TODO
        List<Integer> quantities = null;   //TODO
        Double priceTotal = null;   //TODO - might need to open an attachment or link to reach the actual data
        DateTime purchaseDate = getDateText(payloadMessage);
        System.out.println("invoiceId text: " + invoiceId);
        System.out.println("purchaseDate text: " + purchaseDate);

        return new ReceiptDTO(retailerName,invoiceId, itemsBought, quantities, priceTotal, purchaseDate);
    }

    private static String getInvoiceId(Message payloadMessage) {
        String subjectTxt = ParserCommons.getSubjectText(payloadMessage);
        return subjectTxt.substring(subjectTxt.indexOf('#')+1); //get the number sequence in "Your Amazon.com order #123-4567890-1234567"
    }

}

package domain.objects.receipt;

import dto.ReceiptDTO;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
@Slf4j
@AllArgsConstructor(onConstructor_ = @Autowired)
public class ReceiptService {

    private ReceiptRepository receiptRepository;


    public ReceiptDTO put(ReceiptDTO receiptDTO) {
        //TODO - check if the same receipt already exists through unique constraints (do we want to return an exception in that case?)
        ReceiptEntity entitySaved = save(receiptDTO);
        return dtoFromEntity(entitySaved);
    }

    //TODO - other service API








    private ReceiptEntity save(ReceiptDTO dto) {
        return save(entityFromDto(dto));
    }

    private ReceiptEntity save(ReceiptEntity receiptEntity) {
        ReceiptEntity entity = receiptRepository.save(receiptEntity);
        log.info("saved new entity: {}", entity);
        return entity;
    }


    private ReceiptDTO dtoFromEntity(ReceiptEntity entity) {
        //TODO - redact any data that doesn't belong to dto (stuff that the user shouldn't see)
        return new ReceiptDTO()
                .setId(entity.getId())
                .setRetailerName(entity.getRetailerName())
                .setInvoiceId(entity.getInvoiceId())
                .setItemsBought(entity.getItemsBought())
                .setQuantities(entity.getQuantities())
                .setPriceTotal(entity.getPriceTotal())
                .setPurchaseDate(entity.getPurchaseDate());
    }

    private ReceiptEntity entityFromDto(ReceiptDTO dto) {
        //TODO - validity checks, resolve stuff with defaults...
        return new ReceiptEntity()
                .setId(dto.getId())
                .setRetailerName(dto.getRetailerName())
                .setInvoiceId(dto.getInvoiceId())
                .setItemsBought(dto.getItemsBought())
                .setQuantities(dto.getQuantities())
                .setPriceTotal(dto.getPriceTotal())
                .setPurchaseDate(dto.getPurchaseDate());
    }
}

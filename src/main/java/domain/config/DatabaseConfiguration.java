package domain.config;


import domain.config.properties.SpringProfiles;
import domain.config.properties.SqlProperties;
import lombok.SneakyThrows;
import lombok.val;
import org.apache.tomcat.jdbc.pool.DataSource;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnMissingBean;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.Statement;

import static domain.config.properties.EnvironmentFields.USE_SQL;

@Configuration
@EnableConfigurationProperties(SqlProperties.class)
public class DatabaseConfiguration {


    // spring behaviour
    public static final String VALIDATION_QUERY = "SELECT 1";
    public static final boolean TEST_WHILE_IDLE = true;
    public static final int TIME_BETWEEN_EVICTION_RUNS_MILLIS = 60000;

    //database setting
    public static final String INITIALIZATION_MODE = "always";
    //    public static final String INIT_SQL = "SET NAMES 'utf8mb4' COLLATE 'utf8mb4_unicode_ci'";
//    public static final String CONNECTION_PROPERTIES = "useUnicode=true;characterEncoding=UTF-8";
    public static final String DRIVER_CLASS_NAME = "org.postgresql.Driver";


    @Autowired
    private SqlProperties sqlProperties;

    @Bean
    @Profile(SpringProfiles.STAGE)    //TODO - place back once the user (you, Hillel. I know you're reading this. jk.) would update his maven profile to have this arg
    @ConditionalOnProperty(name = USE_SQL)
    @ConditionalOnMissingBean
    @SneakyThrows
    public DataSource stageDataSource() {
        DatabaseConnection dbEndpoint = new DatabaseConnection()
                .setUrl("jdbc:postgresql://localhost:5432/postgres")
                .setUsername("postgres")
                .setPassword("local");

        return getForEndpointInfo(dbEndpoint);
    }

    //TODO
//    @Bean
//    @Profile(SpringProfiles.PRODUCTION)
//    @ConditionalOnProperty(name = USE_SQL)
//    @ConditionalOnMissingBean
//    public DataSource prodDataSource() {
//        DatabaseConnection dbEndpoint = new DatabaseConnection()
//                .setUrl("enpoint/scheme-prod")  //TODO
//                .setUsername("admin-something")
//                .setPassword("1234");
//
//        return getForEndpointInfo(dbEndpoint);
//    }


    private DataSource getForEndpointInfo(DatabaseConnection dbEndpointInfo) {
        DataSource dataSourceTomcat = new DataSource();
        dataSourceTomcat.setUrl(dbEndpointInfo.getUrl());
        dataSourceTomcat.setUsername(dbEndpointInfo.getUsername());
        dataSourceTomcat.setPassword(dbEndpointInfo.getPassword());

        dataSourceTomcat.setInitialSize(sqlProperties.getInitialSize());
        dataSourceTomcat.setMaxActive(sqlProperties.getMaxActive());
        dataSourceTomcat.setMinIdle(sqlProperties.getMinIdle());
        dataSourceTomcat.setMaxIdle(sqlProperties.getMaxIdle());
        dataSourceTomcat.setValidationQuery(VALIDATION_QUERY);
        dataSourceTomcat.setTestWhileIdle(TEST_WHILE_IDLE);
        dataSourceTomcat.setMinEvictableIdleTimeMillis(TIME_BETWEEN_EVICTION_RUNS_MILLIS);
//        dataSourceTomcat.setInitSQL(INIT_SQL);
//        dataSourceTomcat.setConnectionProperties(CONNECTION_PROPERTIES);
        dataSourceTomcat.setDriverClassName(DRIVER_CLASS_NAME);

        return dataSourceTomcat;
    }

}
package domain.objects.receipt;


import dto.ReceiptDTO;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;

import static org.springframework.http.HttpStatus.OK;

@CrossOrigin(origins = "*") //FIXME - leave it here if only it's browser-accessible, replace '*' with specific ip
@Transactional
@RestController
@RequestMapping("/receipt")
@AllArgsConstructor(onConstructor_ = @Autowired)
@Slf4j  //logger - call with "log.[info/warn/...]()"
public class ReceiptController {

    private ReceiptService receiptService;

    @PutMapping(value = "/")
    @ResponseStatus(OK)
    //TODO - security check
    public ReceiptDTO putReceipt(@RequestBody ReceiptDTO receiptDTO) {
        log.info("Called put on Receipt {}", receiptDTO);
        return receiptService.put(receiptDTO);
    }
}

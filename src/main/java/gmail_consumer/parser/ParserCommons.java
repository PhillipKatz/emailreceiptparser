package gmail_consumer.parser;

import com.google.api.client.util.DateTime;
import com.google.api.services.gmail.model.Message;
import lombok.SneakyThrows;

import java.text.SimpleDateFormat;

public class ParserCommons {

    private static final SimpleDateFormat GMAIL_DATE_FORMATTER = new SimpleDateFormat("EEE, dd MMM yyyy HH:mm:ss Z");

    public static String getSubjectText(Message payloadMessage) {
        return getContentsForHeaderName(payloadMessage, "Subject");
    }
    @SneakyThrows
    public static DateTime getDateText(Message payloadMessage) {
        String dateText = getContentsForHeaderName(payloadMessage, "Date");
        long time = GMAIL_DATE_FORMATTER.parse(dateText).getTime();
        return new DateTime(time);
    }
    private static String getContentsForHeaderName(Message payloadMessage, String headerName) {
        return payloadMessage.getPayload().getHeaders().stream()
                .filter(headerPart -> headerPart.getName().equals(headerName))
                .findAny()
                .get()   //TODO - handle not found
                .getValue();
    }

}

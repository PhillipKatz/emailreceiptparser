package gmail_consumer;

import com.google.api.client.auth.oauth2.Credential;
import com.google.api.client.extensions.java6.auth.oauth2.AuthorizationCodeInstalledApp;
import com.google.api.client.extensions.jetty.auth.oauth2.LocalServerReceiver;
import com.google.api.client.googleapis.auth.oauth2.GoogleAuthorizationCodeFlow;
import com.google.api.client.googleapis.auth.oauth2.GoogleClientSecrets;
import com.google.api.client.googleapis.javanet.GoogleNetHttpTransport;
import com.google.api.client.http.javanet.NetHttpTransport;
import com.google.api.client.json.JsonFactory;
import com.google.api.client.json.gson.GsonFactory;
import com.google.api.client.util.store.FileDataStoreFactory;
import com.google.api.services.gmail.Gmail;
import com.google.api.services.gmail.GmailScopes;
import com.google.api.services.gmail.model.ListMessagesResponse;
import com.google.api.services.gmail.model.Message;
import dto.ReceiptDTO;
import lombok.SneakyThrows;
import lombok.val;

import java.io.*;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

import static gmail_consumer.Retailer.AMAZON;

public class GmailOAuthAccess {
    private static final String APPLICATION_NAME = "Gmail Receipt Reader";
    private static final JsonFactory JSON_FACTORY = GsonFactory.getDefaultInstance();  //Jackson option: JacksonFactory.getDefaultInstance();
    private static final String TOKENS_DIRECTORY_PATH = "tokens";
    private static final List<String> SCOPES = Collections.singletonList(GmailScopes.GMAIL_READONLY);   //if you change the scope, DELETE your current tokens!
    private static final String CREDENTIALS_FILE_PATH = "/credentials.json";    //TODO - get the credentials from somewhere else, not the project's structure


    @SneakyThrows
    // Creates and returns an authorized Credential object.
    private static Credential getCredentials(final NetHttpTransport HTTP_TRANSPORT) {
        // Load client secrets.
        val clientSecrets = getClientSecrets(CREDENTIALS_FILE_PATH);

        // Build flow and trigger user authorization request.
        GoogleAuthorizationCodeFlow flow = new GoogleAuthorizationCodeFlow.Builder(HTTP_TRANSPORT, JSON_FACTORY, clientSecrets, SCOPES)
                .setDataStoreFactory(new FileDataStoreFactory(new File(TOKENS_DIRECTORY_PATH)))
                .setAccessType("offline")
                .build();
        LocalServerReceiver receiver = new LocalServerReceiver.Builder().setPort(8888).build();
        return new AuthorizationCodeInstalledApp(flow, receiver).authorize("user");
    }
    @SneakyThrows
    private static GoogleClientSecrets getClientSecrets(String credentialsFilePath) {
        InputStream in = GmailOAuthAccess.class.getResourceAsStream(credentialsFilePath);
        if (in == null) { throw new FileNotFoundException("Resource not found for file: " + credentialsFilePath); }

        return GoogleClientSecrets.load(JSON_FACTORY, new InputStreamReader(in));
    }

    @SneakyThrows
    private static Message queryPayloadMessageFromIndexMessage(Gmail service, String user, Message messageWithId) {
        /*TODO -
        *  it seems that we can't pull more than 1 message at a time (as seen here: https://developers.google.com/gmail/api/v1/reference/users/messages/get )
        *  so pull those using some queue, don't call this function without proper closure for each request*/
        return service.users().messages().get(user, messageWithId.getId()).execute();
    }

    @SneakyThrows
    public static ReceiptDTO getReceiptsToSave(Retailer currentRetailer) {
        // Build a new authorized API client service.
        final NetHttpTransport HTTP_TRANSPORT = GoogleNetHttpTransport.newTrustedTransport();
        Gmail service = new Gmail.Builder(HTTP_TRANSPORT, JSON_FACTORY, getCredentials(HTTP_TRANSPORT))
                .setApplicationName(APPLICATION_NAME)
                .build();

        // Print the labels in the user's account.
        String user = "me";
        String query = "from: " + currentRetailer.receiptSender; //TODO - don't go over ALL the messages. Look for them by date range (assuming that until a certain date we've covered everything)
        //TODO - handle a false positive (for example, if the receipt-sending-email sometimes sends other stuff).
        ListMessagesResponse messageIndices = service.users().messages().list(user).setQ(query).execute();  //TODO - check if there's a need to go over next page (do this: request.PageToken = response.NextPageToken;)
        System.out.println("found " + messageIndices.getMessages().size() + " entries");
        List<Message> messageBodies = Collections.singletonList(messageIndices.getMessages().get(0))    //TODO - do more than 1 message. Save the indexing messages into a "todo" queue in database (or anywhere non-volatile).
//                messageIndices.getMessages()
                .stream()
                .map(message -> queryPayloadMessageFromIndexMessage(service, user, message))
                .collect(Collectors.toList());

        return currentRetailer.parserInstance.getReceiptForMessage(messageBodies.get(0));

    }
}
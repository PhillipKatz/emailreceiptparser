package gmail_consumer;

import gmail_consumer.parser.AmazonReceiptParser;
import gmail_consumer.parser.NeweggReceiptParser;
import gmail_consumer.parser.RetailerParser;
import lombok.AllArgsConstructor;

@AllArgsConstructor
public enum Retailer {

    AMAZON("Amazon", "auto-confirm@amazon.com", 9001, new AmazonReceiptParser()),
    NEWEGG("newegg", "info@newegg.com", 300, new NeweggReceiptParser());
    //TODO - add to the list

    public final String trademark;
    public final String receiptSender;
    public final long notoriety;    //for GUI and ordering preferences. Needs to be updated via some statistics someone else collected
    public final RetailerParser parserInstance;
}

package domain.objects.receipt;

import com.google.api.client.util.DateTime;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

import javax.persistence.*;
import java.time.LocalDateTime;
import java.util.List;

@Entity
@Table(
        name = "receiptEntity"
        ,uniqueConstraints = @UniqueConstraint(columnNames = {"retailerName", "invoiceId"})  //FIXME - if you want to know why the comma is here; it's so that this line could be commented out without bricking this annotation

        ,indexes =   {
                @Index(name = "IDX_PRICE_TOTAL", columnList = "priceTotal"),
                @Index(name = "IDX_RETAILER_NAME_TO_INVOICE_ID", columnList = "retailerName, invoiceId")
        //TODO - add more
        }
)
@Data
@Accessors(chain = true)
@NoArgsConstructor
public class ReceiptEntity {

    @Id
    @GeneratedValue
    Long id = null;
    String retailerName;    //retailer + invoice are unique together (see above)
    String invoiceId;
    @ElementCollection(targetClass=Long.class)
    List<Long> itemsBought; //points to Ids in item table(s)
    @ElementCollection(targetClass=Integer.class)
    List<Integer> quantities;   //corresponds with items bought
    Double priceTotal;
    DateTime purchaseDate;

}

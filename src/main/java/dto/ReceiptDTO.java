package dto;

import com.google.api.client.util.DateTime;
import lombok.*;
import lombok.experimental.Accessors;
import lombok.experimental.FieldDefaults;

import java.util.List;

/**
 * can technically extend from the DAO to avoid code duplication for fields
 * but then you won't be able to separate the client and server programs (the client needs to know the DTO too)
 */
@Data
@FieldDefaults(level = AccessLevel.PRIVATE)
@Accessors(chain = true)
@AllArgsConstructor
@NoArgsConstructor
public class ReceiptDTO {

    Long id = null;
    String retailerName;
    String invoiceId;
    List<Long> itemsBought;   //TODO - have a table of items (optimally there'd be more than 1 table, but let's not get too ahead of ourselves) and make this list a many-to-one connection (point to the item ids)
    List<Integer> quantities;   //corresponds with items bought
    Double priceTotal;
    DateTime purchaseDate;

    public ReceiptDTO(String retailerName, String invoiceId, List<Long> itemsBought, List<Integer> quantities, Double priceTotal, DateTime purchaseDate) {
        this.retailerName = retailerName;
        this.invoiceId = invoiceId;
        this.itemsBought = itemsBought;
        this.quantities = quantities;
        this.priceTotal = priceTotal;
        this.purchaseDate = purchaseDate;
    }
}

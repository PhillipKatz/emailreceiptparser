package domain.config;

import lombok.Data;
import lombok.experimental.Accessors;

@Data
@Accessors(chain = true)
public class DatabaseConnection {
    private String url;
    private String username;
    private String password;
}

package domain.config.properties;

public class SpringProfiles {

    public static final String STAGE = "stage";
    public static final String PRODUCTION = "production";
}

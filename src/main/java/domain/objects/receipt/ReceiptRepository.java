package domain.objects.receipt;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface ReceiptRepository extends JpaRepository<ReceiptEntity, Long> {

    List<ReceiptEntity> findByRetailerName(String retailerName);

    //TODO - place in the other necessary functions, like bulk-storage, complicated lookups, conditional deletions, etc.

}

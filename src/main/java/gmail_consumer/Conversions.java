package gmail_consumer;

import com.google.api.client.util.Base64;
import com.google.api.client.util.StringUtils;

public class Conversions {

    public static String mimeToUtf8(String mimeStr) {
        return StringUtils.newStringUtf8(Base64.decodeBase64(mimeStr));
    }

}

import domain.objects.receipt.ReceiptEntity;
import domain.objects.receipt.ReceiptService;
import gmail_consumer.GmailOAuthAccess;
import gmail_consumer.Retailer;
import lombok.val;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

import java.util.TimeZone;

import static gmail_consumer.Retailer.AMAZON;

@SpringBootApplication
@EnableJpaRepositories(basePackages = {"domain"})
@EntityScan(basePackages = {"domain"})
@ComponentScan(basePackages = {"domain"})
public class GmailParserPOC implements ApplicationContextAware {

    private static ApplicationContext ac;   //TODO - discard this thing. it's only used to call a bean at the middle of runtime (since this is both a client and server app)

    public static void main(String[] args) {
        TimeZone.setDefault(TimeZone.getTimeZone("UTC"));
        SpringApplication.run(GmailParserPOC.class, args);

        //"client side"
        //TODO - iterate over all appropriate retailers, and get this as an argument.
        Retailer currentRetailer = AMAZON;

        //TODO - don't just put this in main, and don't expect exactly 1 item to return
        val receiptFound = GmailOAuthAccess.getReceiptsToSave(currentRetailer);

        //"server side"
        //TODO - "convert" the items found to our indexed items (look them up online if we don't have them listed)
        //TODO - the following is just what's called after a cron (or other service) picks up an item from a queue

        ReceiptService receiptService = (ReceiptService) ac.getBean("receiptService");
        receiptService.put(receiptFound);

    }

    @Override
    public void setApplicationContext(ApplicationContext ac) {
        this.ac = ac;
    }

}


package gmail_consumer.parser;

import com.google.api.services.gmail.model.Message;
import dto.ReceiptDTO;

public interface RetailerParser {
    public ReceiptDTO getReceiptForMessage(Message payloadMessage);

    //TODO - collect the common operations in one place, so that the strategy won't have duplicate code
}

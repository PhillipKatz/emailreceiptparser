package domain.config.properties;


import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;

@Data
@ConfigurationProperties(prefix = "spring.datasource.tomcat")
public class SqlProperties {


    //    The initial number of connections that are created when the pool is started
    private int initialSize;

    //    Number of active connections that can be allocated from this pool at the same time
    private int maxActive;

    //    Number of connections that should be kept in the pool at all times
    private int maxIdle;
    private int minIdle;


}

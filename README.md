# README #

### How do I run this? ###

* Open the project with IntelliJ
* Let Maven "Import Changes"
* Have PostgreSQL installed (not only PGAdmin, you need Postgres server). Make sure the details match the settings in DatabaseConfiguration.java
* place a valid google credentials file (name it "credentials.json") in "resources" (in folder "main", in case you need to recreate it)
* Run "GmailParserPOC"

### What will the program do? ###
* OAuth you through Google (fill in the username/password in the browser pop-up)
  * if you already OAuth once, your credentials will be saved in "tokens/StoredCredential"
* take the first Amazon purchase message it'll find, ask gmail for its contents
* parse out a couple of data items
* save the parsed data into a local PostgreSQL DB